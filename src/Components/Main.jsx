/* eslint-disable react/destructuring-assignment */
import React, { useState } from "react";
// import styles from './Banner.css'
import { makeStyles } from "@material-ui/core/styles";
import { ReactBingmaps } from "react-bingmaps";
import CordinatesForm from "./CordinatesForm";
import { BING_KEY } from "../EnvVariables";

const useStyles = makeStyles({
  mainContaier: {
    display: "flex"
  },
  mapContainer: {
    height: "100vh",
    width: "-webkit-fill-available"
  }
});

const Main = () => {
  const classes = useStyles();
  const [pins, setPins] = useState([]);

  const onAddPin = (lat, long) => {
    const newPins = pins.concat([[lat, long]]);
    setPins(newPins);
  };
  console.log(BING_KEY);

  const polyline = {
    location: [...pins, pins[0]], // connect first dot to last dot
    option: {
      strokeThickness: 2
    }
  };

  return (
    <div className={classes.mainContaier}>
      <CordinatesForm onAddPin={onAddPin} />
      <div className={classes.mapContainer}>
        <ReactBingmaps
          bingmapKey="BING_KEY"
          mapTypeId="road"
          polyline={polyline}
        >
          {" "}
        </ReactBingmaps>
      </div>
    </div>
  );
};

export default Main;
