/* eslint-disable react/destructuring-assignment */
import React, { useState } from "react";
import { TextField, Button } from "@material-ui/core";
// import styles from './Banner.css'
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  cordinatesFormContaier: {
    display: "flex",
    flexDirection: "column",
    margin: "0 50px"
  },
  title: {
    fontSize: "20px",
    marginBottom: "20px"
  },
  label: {
    marginBottom: "20px"
  }
});

const CordinatesForm = props => {
  const classes = useStyles();
  const [lat, setLat] = useState("");
  const [long, setLong] = useState("");
  const { onAddPin } = props;

  const onClickAdd = () => {
    onAddPin(Number(lat), Number(long));
  };

  return (
    <div className={classes.cordinatesFormContaier}>
      <span className={classes.title}>Cordinates Form</span>
      <TextField
        label="lat"
        className={classes.label}
        value={lat}
        onChange={e => setLat(e.target.value)}
      />
      <TextField
        label="long"
        className={classes.label}
        value={long}
        onChange={e => setLong(e.target.value)}
      />
      <Button variant="contained" color="primary" onClick={onClickAdd}>
        {" "}
        Send
      </Button>
    </div>
  );
};

export default CordinatesForm;
